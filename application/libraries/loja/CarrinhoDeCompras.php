<?php

class CarrinhoDeCompras{
    private $lista_de_produtos = array();

    public function quantidadeDeProdutos(){
        return count($this->lista_de_produtos);
    }

    public function adiciona(Produto $produto){
        $this->lista_de_produtos[] = $produto;
    }

    public function getProduto($index){
        return $this->lista_de_produtos[$index];
    }

    public function esvazia(){
        $this->lista_de_produtos = array();
    }

    public function remove(Produto $produto){

        for($i = 0; $i < count($this->lista_de_produtos); $i++){
            if($this->lista_de_produtos[$i] == $produto){
                array_splice($this->lista_de_produtos, $i, 1);
                return true;
            }
        }

        return false;
    }

    public function finalizaCompra(){
        $total = 0; 

        for($i = 0; $i < count($this->lista_de_produtos); $i++){
            $total += $this->lista_de_produtos[$i]->getPreco();
        }
        return $total;
    }
}