<?php

class Livro{

    private $db;

    private $titulo;
    private $subtitulo;
    private $editora;
    private $autor;
    private $ano;
    private $paginas;
    private $preco;

    function __construct(){
        $ci = & get_instance();
        $this->db = $ci->db;
    }


    public function getAll(){
        $res = $this->db->get('livro');
        return $res->result();
    }

    public function getById($id){
        $res = $this->db->get_where('livro', "id = $id");
        return $res->row();
    }

    public function write(){
        $data['titulo'] = $this->titulo;
        $data['subtitulo'] = $this->subtitulo;
        $data['editora'] = $this->editora;
        $data['autor'] = $this->autor;
        $data['paginas'] = $this->paginas;
        $data['ano'] = $this->ano;
        $data['preco'] = $this->preco;
        $this->db->insert('livro', $data);
    }

    public function update($v, $id){
        $this->db->update('livro', $v, "id = $id");
    }

    public function setTitulo($titulo){
        $this->titulo = $titulo;
    }

    public function setSubtitulo($subtitulo){
        $this->subtitulo = $subtitulo;
    }

    public function setEditora($editora){
        $this->editora = $editora;
    }

    public function setAutor($autor){
        $this->autor = $autor;
    }

    public function setNumPaginas($paginas){
        $this->paginas = $paginas;
    }

    public function setAno($ano){
        $this->ano = $ano;
    }

    public function setPreco($preco){
        $this->preco = $preco;
    }
}