<?php

class Produto{

    private $nome;
    private $preco;
    private $codigo;
    private $descricao;

    function __construct($nome, $preco = 0){
        $this->preco = $preco;
        $this->nome = $nome;
    }

    public function setPreco($preco){
        $this->preco = $preco;
    }

    public function getPreco(){
        return $this->preco;
    }

    public function getNome(){
        return $this->nome;
    }

    public function setNome($nome){
        $this->nome = $nome;
    }

    public function getCodigo(){
        return $this->codigo;
    }

    public function setCodigo($codigo){
        if($this->codigo == null)
            $this->codigo = $codigo;
    }
}