<?php

class Table{

    /** vetor de rótulos da tabela */
    private $labels;

    /** matriz de dados da tabela */
    private $data;

    private $table_class_list = array();
    private $header_class_list = array();

    function __construct($data, $labels){
        $this->labels = $labels;
        $this->data = $data;
    }

    public function useBorder(){
        $this->table_class_list[] = 'table-bordered';
    }

    public function useStripes(){
        $this->table_class_list[] = 'table-striped';
    }

    public function useHover(){
        $this->table_class_list[] = 'table-hover';
    }

    public function useDefaultRow(){
        $this->table_class_list[] = 'table-sm';
    }

    public function setHeaderColor($color){
        $this->header_class_list[] = $color;
    }

    private function getHeaderClassList(){
        return implode(' ', $this->header_class_list);
    }

    /**
     * Gera o cabeçalho da tabela
     * 
     * @return string: código html
     */
    private function getHeader(){
        $html = '<thead class='.$this->getHeaderClassList().'><tr>';
        foreach($this->labels AS $rotulo)
            $html .= "<th>$rotulo</th>";
        return $html . '</tr></thead>';
    }

    private function getBody(){
        $html = '<tbody>';
        foreach($this->data AS $linha){
            $html .= '<tr>';
            foreach($linha AS $celula)
                $html .= "<td>$celula</td>";
            $html .= '</tr>';
        }
        return $html.'</tbody>';
    }

    private function getClassList(){
        return implode(' ', $this->table_class_list);
    }

    /**
     * Gera a tabela completa
     * 
     * @return string: código html
     */
    public function getHTML(){
        $html = '<table class= "table '.$this->getClassList().'">'; 
        $html .= $this->getHeader();
        $html .= $this->getBody();
        return $html;
    }
}