<?php
include_once 'Observer.php';

class Observable{

    private $lista = array();

    public function addObserver(Observer $observer){
        $this->lista[] = $observer;
    }

    public function notify($change){
        foreach ($this->lista as $observer) {
            $observer->update($change);
        }
    }
}