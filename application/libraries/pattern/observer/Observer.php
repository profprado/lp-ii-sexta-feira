<?php

interface Observer{

    function update($change);

}