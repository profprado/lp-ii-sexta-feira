<?php
include_once 'Observable.php';

class PontoDeVendas extends Observable{

    public function executaVenda($produto){
        $this->notify("Venda: $produto");
    }

    public function cancelaVenda($produto){
        $this->notify("Cancelamento: $produto");
    }

    public function trocaProduto($produto){
        $this->notify("Troca: $produto");
    }

}