<?php

class Estoque implements Observer{

    public function entrada($produto){
        // código omitido por preguiça...
    }

    public function saida($produto){
        // código omitido por preguiça...
    }

    public function update($change){
        echo "Estoque recebeu $change<br>";
    }
}