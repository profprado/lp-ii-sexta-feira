<?php
include_once 'Carro.php';

abstract class CarroComplemento implements Carro{

    protected $carro;

    function __construct(Carro $carro){
        $this->carro = $carro;
    }

    abstract function descricao();
    abstract function preco();

}