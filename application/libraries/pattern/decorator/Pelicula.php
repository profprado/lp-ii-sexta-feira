<?php
include_once 'CarroComplemento.php';

class Pelicula extends CarroComplemento{

    function __construct(Carro $carro){
        parent::__construct($carro);
    }

    public function descricao(){
        return $this->carro->descricao().', com insulfim';
    }

    public function preco(){
        return $this->carro->preco() + 550;
    }

}