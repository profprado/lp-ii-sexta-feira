<?php
include_once 'Carro.php';

class CarroBasico implements Carro{

    private $preco;

    function __construct($preco){
        $this->preco = $preco;
    }

    public function descricao(){
        return 'Carro basico';
    }

    public function preco(){
        return $this->preco;
    }

}