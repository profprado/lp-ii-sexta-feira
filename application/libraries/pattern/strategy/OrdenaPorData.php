<?php
include_once 'Ordenador.php';

class OrdenaPorData extends Ordenador{

    public function compara($c1, $c2){
        return strcmp($c1->getData(), $c2->getData());
    }

}