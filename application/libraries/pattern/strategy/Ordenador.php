<?php

// uma interface define responsabilidades a quem a implementa
// todos os métodos definidos em interfaces são abstratos

abstract class Ordenador{

    // Responsabilidade 1. fazer comparações entre os objetos
    abstract function compara($a, $b);


    // Responsabilidade 2. fazer a ordenação por algum meio
    public function ordena($lista){
        usort($lista, array($this, "compara"));
        return $lista;
    }

}