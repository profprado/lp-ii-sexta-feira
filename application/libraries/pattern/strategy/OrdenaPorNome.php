<?php
include_once 'Ordenador.php';

class OrdenaPorNome extends Ordenador{

    public function compara($c1, $c2){
        return strcmp($c1->getNome(), $c2->getNome());
    }

}