<?php
include_once 'Ordenador.php';

class OrdenaPorValor extends Ordenador{

    public function compara($c1, $c2){
        return $c1->getValor() > $c2->getValor();
    }

}