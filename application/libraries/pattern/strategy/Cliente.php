<?php

class Cliente {
    private $nome;
    private $data_da_compra;
    private $valor_da_compra;

    function __construct($nome, $data, $valor){
        $this->nome = $nome;
        $this->data_da_compra = $data;
        $this->valor_da_compra = $valor;
    }

    function getNome(){
        return $this->nome;
    }

    function getData(){
        return $this->data_da_compra;
    }

    function getValor(){
        return $this->valor_da_compra;
    }

    function __toString(){
        return $this->nome . ' - ' .
        $this->data_da_compra . ' - '.
        $this->valor_da_compra;    
    }
}