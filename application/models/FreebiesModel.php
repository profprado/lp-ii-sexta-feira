<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/component/ImageRow.php';

class FreebiesModel extends CI_Model{

    public function get_rows(){
        $html = '';

        $row = new ImageRow('Comece a fazer o trabalho!', '01/03/2018');
        $row->set_description('Descricao da imagem');
        $row->set_image_url('https://mdbootstrap.com/img/Photos/Slides/img%20(99).jpg');
        $row->set_link('http://...');
        $html .= $row->get_html();

        $row = new ImageRow('Nao deixe pra ultima hora!', '02/03/2018');
        $row->set_description('Descricao da imagem');
        $row->set_image_url('https://mdbootstrap.com/img/Photos/Slides/img%20(98).jpg');
        $row->set_link('http://...');
        $html .= $row->get_html();

        $row = new ImageRow('Quem avisa, amigo eh!', '03/03/2018');
        $row->set_description('Descricao da imagem');
        $row->set_image_url('https://mdbootstrap.com/img/Photos/Slides/img%20(97).jpg');
        $row->set_link('http://...');
        $html .= $row->get_html();

        $row = new ImageRow('Fica esperto(a)!', '04/03/2018');
        $row->set_description('Descricao da imagem');
        $row->set_image_url('https://mdbootstrap.com/img/Photos/Slides/img%20(96).jpg');
        $row->set_link('http://...');
        $html .= $row->get_html();

        $row = new ImageRow('Para tudo e faz o trabalho!', '05/03/2018');
        $row->set_description('Descricao da imagem');
        $row->set_image_url('https://mdbootstrap.com/img/Photos/Slides/img%20(95).jpg');
        $row->set_link('http://...');
        $html .= $row->get_html();

        return $html;
    }

    private function get_row($v){
        $row = new ImageRow($v->title, $v->date);
        $row->set_description($v->descricao);
        $row->set_image_url($v->img);
        $row->set_link($v->link);
        return $row->get_html();
    }
}