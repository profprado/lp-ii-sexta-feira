<?php 

class MenuModel extends CI_Model{

    private $table = 'menu';

    public function getMenuList($parent = 0){
        $this->db->order_by('menu_order');
        $res = $this->db->get_where($this->table, array('parent' => $parent, 'deleted' => 0));
        $list = $res->result();

        if(sizeof($list) > 0 ){
            $json = '';
            foreach($list AS $item){
                $aux = '{"id": '.$item->id.', "name":"'.$item->name.'", "link":"'.$item->link.'"';
                $chi = $this->getMenuList($item->id);
                if(strlen($chi) > 0) {
                    $aux .= ', "children": [';
                    $aux .= $chi . ']';
                }
                $aux .= '}';
                $json .= $aux.',';
            }
            return substr($json, 0, strlen($json) - 1);
        }
        return '';
    }

    public function updateMenuItem(){
        $this->form_validation->set_rules('item', 'item movido do menu', 'required|is_natural');
        $this->form_validation->set_rules('parent', 'novo pai do item movido', 'required|is_natural');
        if($this->form_validation->run()){
            $data = $this->input->post();
            $this->updateItem($data);
        }
        else return validation_errors();
    }

    // Isso deve ficar em uma library
    private function updateItem($data){
        $id = $data['item'];
        $parent = $data['parent'];
        $this->db->update($this->table, array('parent' => $parent), "id = $id");
    }

    public function updateMenuName(){
        $this->form_validation->set_rules('id', 'id do item de menu', 'required|is_natural');
        $this->form_validation->set_rules('name', 'label do item de menu', 'required|min_length[3]|max_length[30]');
        if($this->form_validation->run()){
            $data = $this->input->post();
            $this->updateName($data);
        }
        else return validation_errors();
    }

    // Isso deve ficar em uma library
    private function updateName($data){
        $id = $data['id'];
        $name = $data['name'];
        $this->db->update($this->table, array('name' => $name), "id = $id");
    }

    
    public function updateMenuOrder(){
        $data = $this->input->post('itens');
        $this->updateOrder($data);
    }

    // Isso deve ficar em uma library
    private function updateOrder($data){
        for($i = 0; $i < count($data); $i++)
            $this->db->update($this->table, array('menu_order' => $i), "id = ".$data[$i]);
    }

    public function getMenuDBTable(){
        
    }
}