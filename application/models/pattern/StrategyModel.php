<?php
include_once APPPATH.'libraries/pattern/strategy/Cliente.php';
include_once APPPATH.'libraries/pattern/strategy/Ordenador.php';


class StrategyModel extends CI_Model{

    private $c1, $c2, $c3, $c4;
    private $ordenador;
    private $lista;

    function __construct(){
        $this->c1 = new Cliente('Joseh', '2018-03-15', 14253.65);
        $this->c2 = new Cliente('Maria', '2018-03-18', 2541.54);
        $this->c3 = new Cliente('John', '2018-02-02', 47158.00);
        $this->c4 = new Cliente('Alvin', '2018-04-27', 6358.36);
        $this->lista = array($this->c1, $this->c2, $this->c3, $this->c4);
    }


    function setOrdenador(Ordenador $ordenador){
        $this->ordenador = $ordenador;
    }


    function getListaCliente(){
        $aux = '';
        $this->lista = $this->ordenador->ordena($this->lista);

        foreach ($this->lista as $cliente) {
            $aux .= $cliente.'<br>';
        }
        return $aux;
    }
}