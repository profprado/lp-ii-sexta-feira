<?php
include_once APPPATH.'libraries/pattern/decorator/CarroBasico.php';
include_once APPPATH.'libraries/pattern/decorator/Pelicula.php';


class DecoratorModel{

    private $carro;

    function __construct(){
        $this->carro = new CarroBasico(18000);
        $this->carro = new Pelicula($this->carro);
    }

    public function showDecorations(){
        $res = $this->carro->descricao().'<br>';
        $res .= $this->carro->preco().'<br>';
        return $res;
    }

}