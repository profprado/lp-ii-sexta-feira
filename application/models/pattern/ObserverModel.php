<?php
include_once APPPATH.'libraries/pattern/observer/PontoDeVendas.php';
include_once APPPATH.'libraries/pattern/observer/Estoque.php';
include_once APPPATH.'libraries/pattern/observer/Entrega.php';
include_once APPPATH.'libraries/pattern/observer/Financeiro.php';

class ObserverModel{

    private $pdv;
    private $estoque;
    private $entrega;
    private $financeiro;

    function __construct(){
        $this->pdv = new PontoDeVendas();
        $estoque = new Estoque();
        $entrega = new Entrega();
        $financeiro = new Financeiro();

        $this->pdv->addObserver($estoque);
        $this->pdv->addObserver($entrega);
        $this->pdv->addObserver($financeiro);
    }

    public function showChanges(){
        $this->pdv->executaVenda('Forno Microondas');
        $this->pdv->cancelaVenda('Ventilador');
        $this->pdv->trocaProduto('Celular');
    }

}