<?php

class MY_Controller extends CI_Controller{

    
    protected function show($html, $titulo, $subtitulo = ''){
        $aux = $this->load->view('freebies/cabecalho', null, true);
        $data['subtitulo'] = $subtitulo;
        $data['titulo'] = $titulo;
        $aux .= $this->load->view('freebies/intro', $data, true);
        $aux .= $html;
        $aux .= $this->load->view('freebies/rodape', null, true);
        echo $aux;
    }

    // todos os métodos que forem definidos nessa classe
    // estarão disponíveis para o sistema de forma padrão

}