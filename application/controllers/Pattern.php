<?php

class Pattern extends MY_Controller{

    public function strategy(){
        $this->load->library('pattern/strategy/OrdenaPorValor', null, 'ordenador');
        //$this->load->library('pattern/strategy/OrdenaPorData', null, 'ordenador');
        //$this->load->library('pattern/strategy/OrdenaPorNome', null, 'ordenador');
        $this->load->model('pattern/StrategyModel', 'strategy');
        $this->strategy->setOrdenador($this->ordenador);
        $html = $this->strategy->getListaCliente();
        $this->show($html, 'Padrao Strategy', 'Algoritmos dinamicos');
    }    


    public function observer(){
        $this->load->model('pattern/ObserverModel', 'observer');
        $html = $this->observer->showChanges();
        $this->show($html, 'Padrao Observer', 'Notificando alteracoes');
    }


    public function decorator(){
        $this->load->model('pattern/DecoratorModel', 'decorator');
        $html = $this->decorator->showDecorations();
        $this->show($html, 'Padrao Decorator', 'Acrescentando responsabilidades');
    }
}