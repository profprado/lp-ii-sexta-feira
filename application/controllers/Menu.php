<?php

class Menu extends CI_Controller{

    function index(){
        $this->load->model('MenuModel', 'model');
        $data['table'] = $this->model->getMenuDBTable();
        $html = $this->load->view('menu/tree', $data, true);
        $this->show($html, 'DS Menu');
    }

    protected function show($html, $titulo, $subtitulo = ''){
        $aux = $this->load->view('freebies/cabecalho', null, true);
        $data['subtitulo'] = $subtitulo;
        $data['titulo'] = $titulo;
        $aux .= $this->load->view('freebies/intro', $data, true);
        $aux .= $html;
        $aux .= $this->load->view('freebies/rodape', null, true);
        echo $aux;
    }    

    function update(){
        $this->load->model('MenuModel', 'model');
        $this->model->updateMenuItem();
    }

    function setLabel(){
        $this->load->model('MenuModel', 'model');
        $this->model->updateMenuName();
    }

    function setOrder(){
        $this->load->model('MenuModel', 'model');
        $this->model->updateMenuOrder();
    }

    function getMenuJsonData(){
        $this->load->model('MenuModel', 'model');
        $menu = '[{"id": 0, "name":"Root", "children":';
        echo $menu.'['.$this->model->getMenuList().']}]';
    }
}