<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Freebies extends CI_Controller {

    public function onecolumn(){
        $this->load->view('freebies/cabecalho');
        $this->load->view('freebies/style');
        $this->load->view('freebies/navbar');
        $this->load->view('freebies/intro');
        
        $this->load->model('FreebiesModel', 'model');
        $html['rows_list'] = $this->model->get_rows();
        $this->load->view('freebies/rows', $html);

        $this->load->view('freebies/pagination');
        $this->load->view('freebies/footer');
        $this->load->view('freebies/rodape');
    }

}








/*$this->load->view('freebies/first-row');
        $this->load->view('freebies/second-row');
        $this->load->view('freebies/third-row');
        $this->load->view('freebies/fourth-row');
        $this->load->view('freebies/fifth-row');*/