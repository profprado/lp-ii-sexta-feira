<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('ClienteModel', 'model');
    }

    // pagina padrao para todos os controladores
    public function index(){
        echo 'Exibe as opcoes da secao cliente';
    }

    public function lista(){
        $html = $this->model->table_content();
        $this->show($html, 'Lista de Clientes', 'Compras de Fevereiro');
    }

    public function cadastro(){
        $this->model->add_client();
        $data['action'] = 'cadastro';
        $html = $this->load->view('cliente/client_form', $data, true);
        $this->show($html, 'Cadastro de Clientes');
    }

    public function edita($id_cliente = 0){
        $this->model->edit_client($id_cliente);
        $data['action'] = 'edita/'.$id_cliente;
        $html = $this->load->view('cliente/client_form', $data, true);
        $this->show($html, 'Editar Cliente');        
    }


    protected function show($html, $titulo, $subtitulo = ''){
        $this->load->view('freebies/cabecalho');
        $data['subtitulo'] = $subtitulo;
        $data['titulo'] = $titulo;
        $aux = $this->load->view('freebies/intro', $data, true);
        $aux .= $html;
        $aux .= $this->load->view('freebies/rodape', null, true);
        echo $aux;
    }

}