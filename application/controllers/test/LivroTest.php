<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
include 'builder/LivroDataBuilder.php';
include APPPATH . '/libraries/loja/Livro.php';

class LivroTest extends MyToast{

    private $builder;
	
	function __construct() {
        parent::__construct('LivroTest');
        $this->builder = new LivroDataBuilder('livro');
    }
    
    // setup
    function _pre(){
        $this->builder->build();
    }

    // tear down
    function _post(){
        $this->builder->flush();
    }

    function test_retorna_todos_os_livros(){
        $livro = new Livro();
        $res = $livro->getAll();
        $num = count($res);
        $this->_assert_equals($num, 3, "Esperado: 3, Recebido: $num");

        $l1 = $res[0];
        $titulo = $l1->titulo;
        $this->_assert_equals($titulo, 'Dom Casmurro', "Esperado: Dom Casmurro, Recebido: $titulo");

        $l2 = $res[1];
        $ano = $l2->ano;
        $this->_assert_equals($ano, 1925, "Esperado: 1925, Recebido: $ano");

        $l3 = $res[2];
        $paginas = $l3->paginas;
        $this->_assert_equals($paginas, 220, "Esperado: 220, Recebido: $paginas");
    }


    function test_retorna_livro_pelo_id(){
        $livro = new Livro();
        $l1 = $livro->getById(2);
        $editora = isset($l1->editora) ? $l1->editora : 'objeto invalido';
        $this->_assert_equals($editora, 'Sextante', "Esperado: Sextante, Recebido: $editora");
    }


    function test_insere_livro_corretamente(){
        $livro = new Livro();
        $livro->setTitulo('Como ficar rico sem trabalhar');
        $livro->setSubtitulo('O manual do preguicoso sem nocao');
        $livro->setEditora('Record');
        $livro->setAutor('Silas Malafaia');
        $livro->setAno(1996);
        $livro->setNumPaginas(154);
        $livro->setPreco(39.90);
        $livro->write();

        $num = count($livro->getAll());
        $this->_assert_equals($num, 4, "Esperado: 4, Recebido: $num");
    }


    function test_atualiza_dados_do_livro(){
        $data['preco'] = 25.60;
        $data['ano'] = 1983;

        $livro = new Livro();
        $livro->update($data, 2);

        $l2 = $livro->getById(2);
        $this->_assert_equals($l2->preco, 25.6, 'E: 25.6, R: '.$l2->preco);
        $this->_assert_equals($l2->ano, 1983, 'E: 1983, R: '.$l2->ano);
    }

}