<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
require_once(APPPATH . '/libraries/loja/CarrinhoDeCompras.php');
require_once(APPPATH . '/libraries/loja/Produto.php');

class CarrinhoDeComprasTest extends MyToast{
	
	function __construct() {
		parent::__construct('CarrinhoDeComprasTest');
    }
    
    // cenário
    function test_carrinho_inicia_vazio(){
        $cdc = new CarrinhoDeCompras();
        $res = $cdc->quantidadeDeProdutos();
        $this->_assert_equals_strict($res, 0, "Recebido: $res; Esperado: 0");
    }
    
    
    function test_carrinho_recebe_produto(){
        $cdc = new CarrinhoDeCompras();
        $p1 = new Produto('Cerveja', 3.5);
        $cdc->adiciona($p1);

        $p2 = $cdc->getProduto(0);
        $this->_assert_true($p1 === $p2);
    }


    function test_carrinho_pode_ser_esvaziado(){
        $cdc = new CarrinhoDeCompras();
        $p1 = new Produto('Cerveja', 3.5);
        $cdc->adiciona($p1);
        $cdc->adiciona($p1);
        $cdc->adiciona($p1);

        $num = $cdc->quantidadeDeProdutos();
        $this->_assert_equals($num, 3, "Esperado: 3; Recebido: $num");

        $cdc->esvazia();
        $num = $cdc->quantidadeDeProdutos();
        $this->_assert_equals_strict($num, 0, "Esperado: 0; Recebido: $num");
    }


    function test_remove_produto_do_carrinho_vazio(){
        $cdc = new CarrinhoDeCompras();
        $p1 = new Produto('CD Anitta in Concert', 5);
        $res = $cdc->remove($p1);
        $this->_assert_false_strict($res, 'Carrinho vazio nao retornou false');
    }


    function test_remove_produto_do_carrinho_unitario(){
        $cdc = new CarrinhoDeCompras();
        $p1 = new Produto('CD Rei Roberto', 15);
        $cdc->adiciona($p1);

        $p2 = new Produto('CD Zeca Pagodinho', 35);

        $res = $cdc->remove($p1);
        $this->_assert_true_strict($res, 'Carrinho vazio retornou false');

        $res = $cdc->remove($p2);
        $this->_assert_false_strict($res, 'Carrinho vazio nao retornou false');
    }


    function test_remove_produto_do_carrinho_cheio(){
        $cdc = new CarrinhoDeCompras();
        $p1 = new Produto('CD Rei Roberto', 15);
        $p2 = new Produto('CD Zeca Pagodinho', 35);
        $p3 = new Produto('CD Beth Carvalho', 45);
        $p4 = new Produto('CD Fundo de Quintal', 55);

        $cdc->adiciona($p1);
        $cdc->adiciona($p2);
        $cdc->adiciona($p3);

        $res = $cdc->remove($p1);
        $this->_assert_true_strict($res, 'Remocao valida retornou false');

        $res = $cdc->remove($p4);
        $this->_assert_false_strict($res, 'Remocao invalida nao retornou false');
    }


    function test_finaliza_compra_carrinho_vazio(){
        $cdc = new CarrinhoDeCompras();
        $res = $cdc->finalizaCompra();
        $this->_assert_equals_strict($res, 0, "Esperado: 0, Recebido: $res");
    }


    function test_finaliza_compra_carrinho_unitario(){
        $cdc = new CarrinhoDeCompras();
        $p1 = new Produto('PHP 4 Absolute Begginers', 45.82);
        $cdc->adiciona($p1);
        $res = $cdc->finalizaCompra();
        $this->_assert_equals_strict($res, 45.82, "Esperado: 45.82, Recebido: $res");
    }

    function test_finaliza_compra_carrinho_cheio(){
        $cdc = new CarrinhoDeCompras();
        $p1 = new Produto('CD Rei Roberto', 15.82);
        $p2 = new Produto('CD Zeca Pagodinho', 35.16);
        $p3 = new Produto('CD Beth Carvalho', 45.02);

        $cdc->adiciona($p1);
        $cdc->adiciona($p2);
        $cdc->adiciona($p3);

        $res = $cdc->finalizaCompra();
        $this->_assert_equals($res, 96, "Esperado: 96, Recebido $res");
    }
}