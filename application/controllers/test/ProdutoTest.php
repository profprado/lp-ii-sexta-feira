<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
require_once(APPPATH . '/libraries/loja/Produto.php');

class ProdutoTest extends MyToast{
	
	function __construct() {
		parent::__construct('ProdutoTest');
	}
	
    // cenário 1
    function test_produto_tem_preco_zero(){
        $p1 = new Produto('Chinelo');
        $preco = $p1->getPreco();
        $this->_assert_equals_strict($preco, 0, "Recebido: $preco; esperado: 0");
    }

    // cenário 2
    function test_nome_produto_pode_ser_alterado(){
        $p1 = new Produto('Chinelo');
        $nome = $p1->getNome();
        $this->_assert_equals($nome, 'Chinelo', "($nome, Chinelo)");

        $p1->setNome('Sandalia');
        $nome = $p1->getNome();
        $this->_assert_equals($nome, 'Sandalia', "($nome, Sandalia)");
    }
    
    function test_preco_produto_pode_ser_alterado(){
        $p1 = new Produto('Chinelo', 123);
        $preco = $p1->getPreco();
        $this->_assert_equals($preco, 123, "($preco, 123)");

        $p1->setPreco(456);
        $preco = $p1->getPreco();
        $this->_assert_equals($preco, 456, "($preco, 456)");
    }

    // cenário 3
    function test_codigo_deve_ser_imutavel(){
        $p1 = new Produto('Camiseta', 100);
        $p1->setCodigo('ovds9498');
        $code = $p1->getCodigo();
        $this->_assert_equals($code, 'ovds9498', "$code -> ovds9498");

        $p1->setCodigo('kvcyt5207');
        $code = $p1->getCodigo();
        $this->_assert_equals($code, 'ovds9498', "$code -> ovds9498");
    }

}