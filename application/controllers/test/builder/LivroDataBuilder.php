<?php

require_once 'TestDataBuilder.php';

class LivroDataBuilder extends TestDataBuilder{

    protected function getData(){
        return array(
            array(
                'titulo' => 'Dom Casmurro',
                'subtitulo' => 'Um heroi nacional',
                'autor' => 'Machado Assis',
                'editora' => 'Nova Lisboa',
                'ano' => 1882,
                'paginas' => 185,
                'preco' => 32.45
            ),
            array(
                'titulo' => 'O Alienista',
                'subtitulo' => '',
                'autor' => 'Dias Gomes',
                'editora' => 'Sextante',
                'ano' => 1925,
                'paginas' => 120,
                'preco' => 21.5
            ),
            array(
                'titulo' => 'A hora da estrela',
                'subtitulo' => '',
                'autor' => 'Cecília Meireles',
                'editora' => 'Saraiva',
                'ano' => 1974,
                'paginas' => 220,
                'preco' => 45.8
            )
        );
    }

}