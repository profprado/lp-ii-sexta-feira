<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	// cada função do controlador gera uma página
	public function index(){
		$this->load->view('welcome_message');
		$this->load->view('hello');
	}

	// cada função do controlador "é uma página".
	public function hello(){
		$this->load->view('hello');
	}

	public function load(){
		echo 'Carregando dados do usuario...';
	}
}
