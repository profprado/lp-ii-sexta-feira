<!--Footer-->
<footer class="page-footer center-on-small-only mdb-color lighten-2">
            <!--Footer links-->
            <div class="container">
                <div class="row my-4">
                    <!--First column-->
                    <div class="col-lg-3 col-md-6">
                        <h6 class="h5-responsive title mb-3">
                            <strong>About material design</strong>
                        </h6>
                        <p>Material Design for Bootstrap (MDB) is a powerful Material Design UI KIT for most popular HTML, CSS,
                            and JS framework - Bootstrap.</p>
                    </div>
                    <!--/.First column-->
                    <hr class="w-100 clearfix d-sm-none">
                    <!--Second column-->
                    <div class="col-lg-2 col-md-6 ml-auto">
                        <h6 class="h5-responsive title mb-3">
                            <strong>First column</strong>
                        </h6>
                        <ul>
                            <li>
                                <a href="#!">Link 1</a>
                            </li>
                            <li>
                                <a href="#!">Link 2</a>
                            </li>
                            <li>
                                <a href="#!">Link 3</a>
                            </li>
                            <li>
                                <a href="#!">Link 4</a>
                            </li>
                        </ul>
                    </div>
                    <!--/.Second column-->
                    <hr class="w-100 clearfix d-sm-none">
                    <!--Third column-->
                    <div class="col-lg-2 col-md-6 ml-auto">
                        <h6 class="h5-responsive title mb-3">
                            <strong>Second column</strong>
                        </h6>
                        <ul>
                            <li>
                                <a href="#!">Link 1</a>
                            </li>
                            <li>
                                <a href="#!">Link 2</a>
                            </li>
                            <li>
                                <a href="#!">Link 3</a>
                            </li>
                            <li>
                                <a href="#!">Link 4</a>
                            </li>
                        </ul>
                    </div>
                    <!--/.Third column-->
                    <hr class="w-100 clearfix d-sm-none">
                    <!--Fourth column-->
                    <div class="col-lg-2 col-md-6 ml-auto">
                        <h6 class="h5-responsive title mb-3">
                            <strong>Third column</strong>
                        </h6>
                        <ul>
                            <li>
                                <a href="#!">Link 1</a>
                            </li>
                            <li>
                                <a href="#!">Link 2</a>
                            </li>
                            <li>
                                <a href="#!">Link 3</a>
                            </li>
                            <li>
                                <a href="#!">Link 4</a>
                            </li>
                        </ul>
                    </div>
                    <!--/.Fourth column-->
                </div>
            </div>
            <!--/.Footer links-->

            <!--Copyright-->
            <div class="footer-copyright">
                <div class="containter-fluid">
                    © 2015 Copyright:
                    <a href="https://www.MDBootstrap.com"> MDBootstrap.com </a>
                </div>
            </div>
            <!--/.Copyright-->
        </footer>
        <!--/.Footer-->
