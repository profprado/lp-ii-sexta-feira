<div class="row">
    <div id="tree1" class="col-md-3" style="border: 1px solid lightgray; border-radius: 5px; padding: 15px" data-url="http://localhost/lp6/index.php/menu/getmenujsondata"></div>
    <div id="table-viewer" class="col-md-12"><?= $table ?></div>
</div>
<script src="<?= base_url('assets/js/tree.jquery.js') ?>"></script>

<script>
    $('#tree1').tree({
        autoOpen: true,
        dragAndDrop: true,
        onCreateLi: function(node, $li) {
            $li.find('.jqtree-element span')
            .attr('data-link', node.link)
            .attr('id', node.id);

            $li.find('.jqtree-element')
            .attr('id', node.id);
        }
    });
</script>

<script src="<?= base_url('assets/js/menumanager.js') ?>"></script>